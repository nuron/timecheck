#!/bin/sh
# This script is released under the GPLv3 license
# See COPYING file for more information

PRG_NAME="Timecheck"
PRG_PATH=$1
DAY=$(LANG=EN_US.UTF-8 date +%a)
WEEK=$(date +%G%V)
USERONLINE=false


checkPermission(){
	INSTALL_USER=$(whoami)
	if [ ! "$INSTALL_USER" = "root" ]
		then
			echo "You don't have enough permissions to perform this script. Please retry as root. Script execution aborted."
			exit 1
	fi
}

readConfigFile(){
	while IFS= read -r line; do
		INPUT=$(echo "$line" | sed 's/[ \t]*//;/^#/d')

		case "$INPUT" in
			USER*) USER="$INPUT"
			;;
			TIME_MOFR*) TIME_MOFR="$INPUT"
			;;
			TIME_SASU*) TIME_SASU="$INPUT"
			;;
			EXIT_SCRIPT*) EXIT_SCRIPT="$INPUT"
			;;
			PRG_PATH*) PRG_PATH="$INPUT"
			;;
			*)
		esac
	done < "$PRG_PATH"timecheck.conf

	USER=$(echo "$USER" | sed 's/[ \t]*//;/^#/d;/=/!d;s#^.*=##')
	TIME_MOFR=$(echo "$TIME_MOFR" | sed 's/[ \t]*//;/^#/d;/=/!d;s#^.*=##')
	TIME_SASU=$(echo "$TIME_SASU" | sed 's/[ \t]*//;/^#/d;/=/!d;s#^.*=##')
	EXIT_SCRIPT=$(echo "$EXIT_SCRIPT" | sed 's/[ \t]*//;/^#/d;/=/!d;s#^.*=##')
	PRG_PATH=$(echo "$PRG_PATH" | sed 's@[ \t]*@@;\@^#@d;\@=@!d;s@^.*=@@')
}

checkInstallerRan(){
	if [ ! -f "$PRG_PATH""timecheck.conf" ]
		then
			echo "Please run install-timecheck.sh first, as described in the README"
			exit 1
	fi
}

createFiles(){
	if [ ! -f "$PRG_PATH""/time_MOFR.txt" ]
		then
			echo "-1" > "$PRG_PATH""/time_MOFR.txt"
	fi
	if [ ! -f "$PRG_PATH""/time_SASU.txt" ]
		then
			echo "-1" > "$PRG_PATH""/time_SASU.txt"
	fi
	if [ ! -f "$PRG_PATH""/week_MOFR.txt" ]
		then
			echo "-1" > "$PRG_PATH""/week_MOFR.txt"
	fi
	if [ ! -f "$PRG_PATH""/week_SASU.txt" ]
		then
			echo "-1" > "$PRG_PATH""/week_SASU.txt"
	fi
}

setPeriod() {
	case "$DAY" in
		Mon|Tue|Wed|Thu|Fri)	TIME_RANGE=MOFR; TIME_INPUT="$PRG_PATH"time_"$TIME_RANGE".txt; WEEK_INPUT="$PRG_PATH"week_"$TIME_RANGE".txt; TIME_AVAILABLE=$TIME_MOFR
		;;
		Sat|Sun)	TIME_RANGE=SASU; TIME_INPUT="$PRG_PATH"time_"$TIME_RANGE".txt; WEEK_INPUT="$PRG_PATH"week_"$TIME_RANGE".txt; TIME_AVAILABLE=$TIME_SASU; TIME_RANGE=SASU
		;;
		*)	echo "Error!" && exit 1
	esac
}

timeLeft() {
	TIME_USED=$(cat "$TIME_INPUT")
	TIME_LEFT=$(( TIME_AVAILABLE - TIME_USED ))
}

logoutUser() {
	passwd -l "$USER"
	pkill -15 -u "$USER"
}

sendNotificationToUser() {
	sudo -u "$USER" DISPLAY="$(who | grep "$USER" | awk '{ print $NF }' | sort -u | sed -e "s/^(//" -e "s/)$//")" DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/"$(id -u "$USER")"/bus notify-send "$@"
}

sendNotification() {
	if [ "$TIME_LEFT" -lt "2" ]
		then
			URGENCY="critical"
	elif [ "$TIME_LEFT" -lt "5" ]
		then
			URGENCY="normal"
	else
		URGENCY="low"
	fi

	if [ "$SEND_NOTIFICATION" = "true" ] && [ "$TIME_LEFT" -gt "3" ]
		then
			sendNotificationToUser -a $PRG_NAME -u $URGENCY "Timecheck" "There are $((TIME_LEFT - 1)) minutes left until you will be logged out!" && SEND_NOTIFICATION=false
		elif [ "$SEND_NOTIFICATION" = "true" ]
			then
				sendNotificationToUser -a $PRG_NAME -u $URGENCY "Timecheck" "You will be logged out in less than a minute!" && SEND_NOTIFICATION=false
	fi
}
isUserOnline() {
	who | grep -q "$USER" && USERONLINE=true
}
notificationDelay() {
	if [ "$TIME_LEFT" = "11" ] || [ "$TIME_LEFT" = "6" ] || [ "$TIME_LEFT" = "2" ]
		then
			SEND_NOTIFICATION=true
	fi
}
increaseUsedTime() {
	TIME_USED=$((TIME_USED + 1))
	echo "$TIME_USED" > "$TIME_INPUT"
	echo "$WEEK" > "$WEEK_INPUT"
}

reset() {
	if [ "$( cat $TIME_INPUT)" -lt "$TIME_AVAILABLE" ]
		then
			passwd -u "$USER"
	elif [ "$WEEK" -gt "$(cat "$WEEK_INPUT")" ] || [ "$WEEK" -lt "$(cat "$WEEK_INPUT")" ]
			then
				echo 0 > "$TIME_INPUT"
				passwd -u "$USER"
	fi


}

checkPath() {
	case $PRG_PATH in
		*/)
		;;
		*) PRG_PATH=$PRG_PATH/
		;;
	esac
}

main() {
	checkPermission
	checkPath
	checkInstallerRan
	readConfigFile
	createFiles
	setPeriod
	reset
	isUserOnline
	if [ "$USERONLINE" = "false" ]
		then
			exit 0
	else
		timeLeft
		increaseUsedTime
	fi
	if [ "$TIME_LEFT" = "1" ]
		then
			logoutUser
			exit 0
	elif [ "$TIME_LEFT" -lt "1" ]
		then
			logoutUser
			exit 0
	fi

	notificationDelay
	sendNotification

}
main
